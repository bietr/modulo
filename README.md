#O Módulo
O módulo designado é o de Formação de Preços.

##Configuração
	Clone este repositório em um diretório chamado "modulo";
	Importe para o seu SGBD o arquivo "lp2_modulo.sql" que pode ser encontrado na pasta "sql";
	Entre na URL: http://localhost/modulo/preco


#### Lista de Produtos

Para que o módulo de Preços funcione corretamente, foi criada uma lista de produtos estática, para garantir que os preços estejam atrelados a alguma coisa, vale ressaltar que o módulo de produtos será desenvolvido por outro aluno.

#### Geração de Preços
	Para gerar um preço, devemos entrar em "localhost/modulo/preco" e clicar em um dos produtos listados;
	Existem dois preços já gerados nos produtos "Leite" e "Bolacha", que podem ser utilizados para testar a edição e a deleção de preços;
	Para gerar um preço, clicamos no botão "NOVO PREÇO" e inserimos o preço de compra, o custo fixo gerado pelo produto e a margem de lucro desejada em cima do custo total, e o preço de compra é calculado sistêmicamente. Entende-se por custo fixo os custos tais como Refrigeração, Transporte, Embalagem e relacionados ao custo do empresário para manter o produto;

####Testes de Unidade da Formação de Preços
	Foi criado um teste para a lista de produtos que se encontra em:
	"http://localhost/modulo/preco/test/ProdutoTest"
	
	Para executar o teste de regressão com todos os testes de integração, acesse:
	"http://localhost/modulo/preco/test/all"

###Autor
_Emanuel Ricardo Biet Resende - GU3001601_
