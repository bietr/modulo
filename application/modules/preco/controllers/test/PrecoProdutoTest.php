<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/preco/libraries/PrecoProduto.php';
include_once APPPATH . 'modules/preco/controllers/test/builder/PrecoProdutoDataBuilder.php';

class PrecoProdutoTest extends Toast{
    private $builder;
    private $preco;

    function __construct(){
        parent::__construct('ProdutoTest');
    }

    function _pre(){
        $this->builder = new PrecoProdutoDataBuilder();
        $this->preco = new PrecoProduto();
    }


    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->preco, "Erro na criação do preço");
    }

    function test_insere_registro_na_tabela(){
        // cenário 1: vetor com dados corretos
        $this->builder->clean_table();
        $data = $this->builder->getData(0);
        $id1 = $this->preco->insert($data);
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");

        // verificação: o objeto criado é, de fato, aquele que enviamos?
        $task = $this->preco->get(array('id' => 1))[0];
        $this->_assert_equals($data['preco'], $task['preco']);
        $this->_assert_equals($data['compra'], $task['compra']);
        $this->_assert_equals($data['preco'], $task['preco']);

        // cenário 2: vetor vazio
        $id2 = $this->preco->insert(array());
        $this->_assert_equals(-1, $id2, "Esperado -1, recebido $id2");

        // cenário 3: vetor com dados inesperados
        $info = $this->builder->getData(1);
        $info['unexpected_col_name'] = 1;
        $id = $this->preco->insert($info);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        // cenário 4: vetor com dados incompletos... deve ser
        // tratado pela validação, mas tem que ser pensado aqui
        $v = array('lucro' => 'vetor incompleto');
        $id = $this->preco->insert($v);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");
    }

    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $tasks = $this->preco->get();
        $this->_assert_equals(3, sizeof($tasks), "Número de registros incorreto");
    }

    function test_carrega_registro_condicionalmente(){
        $this->builder->clean_table();
        $this->builder->build();

        $price = $this->preco->get(array('compra' => 3, 'produto_id' => 1))[0];
        $this->_assert_equals(3, $price['compra'], "Erro no preço de compra");
        $this->_assert_equals(1, $price['produto_id'], "Erro no id do produto");
    }
    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();

        // lê um registro do bd
        $price1 = $this->preco->get(array('id' => 1))[0];
        $this->_assert_equals(3, $price1['compra'], "Erro no preço de compra");
        $this->_assert_equals(1, $price1['produto_id'], "Erro no id do produto");

        // atualiza seus valores
        $price1['compra'] = 4;
        $price1['produto_id'] = 8;
        $this->preco->insert_or_update($price1);

        // lê novamente, o mesmo objeto, e verifica se foi atualizado
        $price2 = $this->preco->get(array('id' => 1))[0];
        $this->_assert_equals($price1['compra'] , $price1['compra'], "Erro no preço de compra");
        $this->_assert_equals($price2['produto_id'], $price2['produto_id'], "Erro no id do produto");
    }

    function test_remove_registro_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        // verifica que o registro existe
        $price1 = $this->preco->get(array('id' => 1))[0];
        $this->_assert_equals(3, $price1['compra'], "Erro no preço de compra");
        $this->_assert_equals(1, $price1['produto_id'], "Erro no id do produto");

        // remove o registro
        $this->preco->delete(array('id' => 1));

        // verifica que o registro não existe mais
        $price2 = $this->preco->get(array('id' => 1));
        $this->_assert_equals_strict(0, sizeof($price2), "Registro não foi removido");
    }
}