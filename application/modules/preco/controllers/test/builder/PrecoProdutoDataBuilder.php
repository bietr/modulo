<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class PrecoProdutoDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_modulo_test'){
        parent::__construct('preco_produto', $table);
    }

    function getData($index = -1){
        $data[0]['id'] = 1;
        $data[0]['compra'] = 3;
        $data[0]['fixo'] = 6;
        $data[0]['lucro'] = 100;
        $data[0]['preco'] = 18;
        $data[0]['produto_id'] = 1;

        $data[1]['id'] = 6;
        $data[1]['compra'] = 2;
        $data[1]['fixo'] = 2;
        $data[1]['lucro'] = 150;
        $data[1]['preco'] = 10;
        $data[1]['produto_id'] = 4;

        $data[2]['id'] = 8;
        $data[2]['compra'] = 2;
        $data[2]['fixo'] = 2;
        $data[2]['lucro'] = 150;
        $data[2]['preco'] = 10;
        $data[2]['produto_id'] = 1;

        return $index > -1 ? $data[$index] : $data;
    }

}