<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . 'controllers/test/Toast.php';
include_once APPPATH . 'modules/preco/libraries/Produto.php';

/**
 * Essa classe de teste é apenas ilustrativa e foi criada somente
 * para que você perceba a utilidade da execução de uma suite de testes.
 */

class ProdutoTest extends Toast{
    private $produto;

    function __construct(){
        parent::__construct('ProdutoTest');
    }

    function _pre(){
        $this->produto = new Produto();
    }

    function test_carrega_lista_de_produtos(){
        $v = $this->produto->lista();
        $this->_assert_equals(4, sizeof($v), "Número de produtos incorreto");
    }

    function test_gera_nome_dos_produtos(){
        $nome = $this->produto->nome(2);
        $this->_assert_equals('Bala de Goma', $nome, "Erro no nome do produto");
    }
}